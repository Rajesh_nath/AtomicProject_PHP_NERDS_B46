<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\City\City;

if(isset($_POST['mark'])) {

    $objCity = new City();
    $objCity->recoverMultiple($_POST['mark']);

    Utility::redirect("index.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashed.php");

}
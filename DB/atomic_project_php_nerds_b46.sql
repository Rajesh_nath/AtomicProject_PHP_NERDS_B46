-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 12, 2017 at 02:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_php_nerds_b46`
--
CREATE DATABASE IF NOT EXISTS `atomic_project_php_nerds_b46` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `atomic_project_php_nerds_b46`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `birth_date` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birth_date`, `soft_deleted`) VALUES
(1, 'Rajesh', '1999-02-15', 'No'),
(2, 'Rasu', '1998-02-18', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Himu1', 'Humayun Ahmed1', 'No'),
(5, 'Himu2', 'Humayun Ahmed2', 'Yes'),
(6, 'dsfdgdg', 'fdgdg', 'No'),
(11, 'dfssf', 'sfd sf', 'No'),
(16, 'Himu3', 'Humayun Ahmed3', 'No'),
(17, 'Himu4', 'Humayun Ahmed4', 'No'),
(18, 'Himu5', 'Humayun Ahmed5', 'No'),
(19, 'Himu6', 'Humayun Ahmed6', 'No'),
(21, 'dsgt', 'erytetr', 'No'),
(22, 'gfdhdsy', 'dsyeyr', 'No'),
(23, 'ftjhdsd', 'dgtsdtrtet', 'No'),
(24, 'fdghshysy', 'fdhhehye', 'No'),
(25, 'hytrsuytes', 'dssyeyre', 'No'),
(26, 'fsjuhhsed', 'ygfdhgdsdr', 'No'),
(27, 'fjhgsdrygtd', 'drtaert', 'No'),
(28, 'fjgssruesuy', 'sdrhyserye', 'No'),
(29, 'iujytedusey', 'ydrsuyrsy', 'No'),
(30, 'fujdrsuru', 'xfdhfdxjhfryf', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `city` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `hobbies` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE IF NOT EXISTS `picture` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `picture` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `picture`
--

INSERT INTO `picture` (`id`, `name`, `picture`, `soft_deleted`) VALUES
(1, '', '1486650968IMG_20150221_132301.png', 'No'),
(2, 'raj', '1486651371butterfly.au   - clean code (1).pdf', 'No'),
(3, 'rajesh', '1486651427booktitlebg.jpg', 'No'),
(4, 'rajesh', '1486651461booktitlebg.jpg', 'No'),
(5, 'dwsda', '1486651553booktitlebg.jpg', 'No'),
(6, 'dsf', '1486651636booktitlebg.jpg', 'No'),
(7, 'dsf', '1486651709booktitlebg.jpg', 'No'),
(8, 'dsf', '1486651899booktitlebg.jpg', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
